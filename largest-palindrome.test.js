const largestPalindrome = require('./largest-palindrome');

let palindromeTest = [
	{value: 101110, result: 101101},
	{value: 800000, result: 793397}
];

palindromeTest.forEach(function(obj, index){
	it('Palindrome', function(){
		expect(largestPalindrome(obj.value).currentValue).toBe(obj.result);
	});
})