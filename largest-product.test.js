const largestProduct = require('./largest-product');

let productTest = [
	{value: 3675356291, group: 5, result: 3150},
	{value: 2709360626, group: 5, result: 0}
];

productTest.forEach(function(obj, index){
	it('Products', function(){
		expect(largestProduct(obj.value, obj.group)).toBe(obj.result);
	});
})