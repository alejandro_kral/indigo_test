/**
 * Find the greatest product of K consecutive digits in the N digit number.
 * @param  {Integer} value
 * @param  {Integer} group Length group
 * @return {String | Integer}
 */
function largestProduct(value, group) {
    if( !isNaN( value ) && Number.isInteger(value) ) {
        let valueString = value.toString();
        let arrValues = [];
        for( let initial = 0; initial < (valueString.length - (group -1) ); initial++ ) {
        	arrValues.push(valueString.substring(initial, (initial + group)));
        }
        arrValues.sort(function(prev, cur){
        	let prevProduct = prev.split('').reduce(function (prevPrev, prevCur){
        		return prevPrev * prevCur;
        	});
        	let curProduct = cur.split('').reduce(function (prevCur, curCur){
        		return prevCur * curCur;
        	});
        	if(prevProduct < curProduct) {
        		return 1;
        	} if( prevProduct > curProduct) {
        		return -1;
        	}
        	return 0;
        });
        return parseInt(arrValues[0].split('').reduce(function (prevCur, curCur){
        		return prevCur * curCur;
        	})
        );
    }
    return 'An error ocurred';
}

module.exports = largestProduct;