# Indigo Test

Código de prueba para el test de Indigo

## Un paso a la vez

El código fue probado en Linux Centos 7 con NodeJs v7.9

### Prerequisitos

Para iniciar el proyecto clonar el repositorio, hacer la instalación con npm, requiere de jest para pruebas unitarias

```
npm i
```

## Pruebas

Para iniciar las pruebas unitarias del código es necesario ejecutar la siguiente línea:

```
npm test
```

### Resultados

Los resultados de largestProduct fueron los siguientes

```
3150
0
```

Los resultados de largestPalindrome fueron los siguientes

```
101101
793397
```

## Autor

* **Alejandro Paredes**

