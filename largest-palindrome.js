/**
 * find the largest palindrome where the number is producto of 3 digits factors
 * @param  {Integer} value number to iterate
 * @return Bool | Object returns an object with the number, and the factors
 */
function largestPalindrome(value) {
	for( let initValue = 0; initValue < value; initValue++) {
		let currentValue = value - initValue;
		let valueString = currentValue.toString();
		let factors = {};
		sqrtValue = parseInt(Math.sqrt(currentValue));
		if( sqrtValue < 100 ) {
			return false;
		}
		if( currentValue === parseInt( valueString.split('').reverse().join('') ) ) {
			if( typeof (factors = products(currentValue)) == "object") {
				return {
					currentValue: currentValue,
					division: factors.division,
					remainer: factors.remainer
				};
			}
		} else {
			continue;
		}
	}
	return false;
}

/**
 * Find the factors where the length is 3 digits
 * @param  {Integer} value where to find its factors
 * @return {Bool|Object}       The factor's value
 */
function products(value) {
	for( let division = parseInt(Math.sqrt(value)); division >= 100; division--) {
		let remainer = value / division;
		let stringDivision = division.toString();
		let stringRemainer = remainer.toString();
		if( Number.isInteger(remainer) && stringDivision.length === 3 && stringRemainer.length === 3  ) {
			return {remainer: remainer, division: division};
		} else {
			continue;
		}
	}
	return false;
}

module.exports = largestPalindrome;